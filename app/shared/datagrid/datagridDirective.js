angular.module('moneymoney').directive('mmDatagrid', [function() {

    var editRow = function(index, row) {
	row.isEditing = true;
    };

    var saveRow = function(index, row) {
	row.isEditing = false;
    };

    var cancelRow = function(index, row) {
	row.isEditing = false;
    };

    var newRow = function(griddata, index, row) {
	griddata.splice(index+1, 0, {});
    };
    
    return {
	templateUrl: 'shared/datagrid/datagridView.html',
	restrict: 'E',
	scope: {
	    columnDefs: '=',
	    gridData: '='
	},
	link: function (scope, element) {
	    scope.editRow = editRow;
	    scope.saveRow = saveRow;
	    scope.cancelRow = cancelRow;
	    scope.newRow = newRow;
	}
    };
}]);
