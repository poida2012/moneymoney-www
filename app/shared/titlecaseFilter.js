/* title case filter */
angular.module('moneymoney').filter('mmTitlecase', [function(){
    return function(input) {
	var result = "";
	
	if (input) {
	    var inSpace = true;
	    
	    input.split("").forEach(function(l) {
		if (l.match(/\s/)) {
		    result += l;
		    
		    inSpace = true;
		} else {
		    if (inSpace) {
			result += l.toUpperCase();
		    } else {
			result += l.toLowerCase();
		    }

		    inSpace = false;
		}
	    });
	}
	
	return result;
    };
}]);
