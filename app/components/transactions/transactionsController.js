angular.module('moneymoney').controller('mmTransactionsCtrl', ['$scope', function($scope) {
    $scope.tablecaption = 'transaction data';
    $scope.tablecolumns = [{name: 'amount', inputtype: 'number'}];
    $scope.tabledata = [{amount: 1.1}, {amount: 2.0}];
}]);
