angular.module('moneymoney').config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
	.state('transactions', {
	    templateUrl: 'components/transactions/transactionsView.html',
	    controller: 'mmTransactionsCtrl'
	})
	.state('admin', {
	    templateUrl: 'components/admin/adminView.html',
	    controller: 'mmAdminCtrl'
	});

  $urlRouterProvider
	.when('/admin', function($state) { $state.transitionTo('admin'); })
    	.when('/transactions', function($state) { $state.transitionTo('transactions'); })
	.otherwise('/transactions');
}]);
